import React, {useState} from 'react'
import api from '../utils/axios'
import useReactRouter from 'use-react-router'

export default function Login(){

    const { history } = useReactRouter()

    const [user, setUser] = useState({})

    return (
        <div className="h-100 d-flex d-flex justify-content-center align-items-center">
            <form className="login-form-size p-3">
                <div class="form-group">
                    <input type="email" className="form-control" name="email" id="exampleInputEmail1" placeholder="email" onChange={(target) => onChange(target, user, setUser)} />
                </div>
                <div class="form-group">
                    <input type="password" className="form-control" name="pass" id="exampleInputPassword1" placeholder="senha" onChange={(target) => onChange(target, user, setUser)} />
                </div>
                <button type="button" className="btn btn-primary" onClick={() => login(user, history)} >Entrar</button>
            </form>
        </div>
    )
}

const onChange = ({target: {name, value}}, values, setValues) => {
    setValues({...values, [name]: value})
}

const login = async (values, history) => {
    if(values.email === ''){
        alert('Preencha o email')
        return
    }

    if(values.pass === ''){
        alert('Preencha a senha')
        return
    }

    const resp = await api.post('/auth/login', values)
    console.log(resp);
    const { data, status } = resp
    if(status === 401){
        alert('Email ou Senha inválidos')
        return
    }

    localStorage.setItem('access_token', data.access_token)
    localStorage.setItem('user_id', data.user_id)
    localStorage.setItem('role', data.role)
    
    history.push('/')
}