export default (values) => {
  let isTrue = true
  const objectKeys = Object.keys(values)

  if(objectKeys < 1){
    return false
  }

  // eslint-disable-next-line no-sequences
  ['name', 'description', 'price'].forEach(element => {
    if(!objectKeys.includes(element)){
      isTrue = false
      return;
    }
  });

  return isTrue
}