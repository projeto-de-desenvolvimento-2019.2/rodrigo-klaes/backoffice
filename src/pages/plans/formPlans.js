import React, { useState, useEffect } from 'react'
import Create from '../../components/create'
import api from '../../utils/axios';
import './css/plan.css'
import validateMapping from './validateMapping';
import useReactRouter from 'use-react-router'

export default function Townhouse({disabled = false, type}){
  const {match: { params: { id } }, history: { goBack } }= useReactRouter()
  const [values, setValues] = useState({})

  useEffect(() => {
    if(id){
      get(id, setValues)
    }
  }, [id])

  const fields = [
    {
      field: 'input', 
      type: 'text', 
      title: 'Nome', 
      id: "name", 
      name: "name", 
      value: values.name || '', 
      disabled, 
      onChange: (target) => onChange(target, values, setValues)
    },
    {
      field: 'input', 
      type: 'text', 
      title: 'Descrição', 
      id: "description", 
      name: "description", 
      value: values.description || '', 
      disabled,  
      onChange: (target) => onChange(target, values, setValues)
    },
    {
      field: 'input',
      type: 'text', 
      title: 'Valor', 
      id: "price", 
      name: "price", 
      value: values.price || '', 
      disabled,  
      onChange: (target) => onChange(target, values, setValues)
    }
  ]

  const buttonsActions = [
    {field: 'button', type: 'button', title: 'Criar Plano', id: "button_criar", label: 'Criar Plano', className:  type !== "create" ? "hide" : "btn btn-primary", onClick: () => create(values, setValues)},
    {field: 'button', type: 'button', title: 'Editar Plano', id: "button_criar", label: 'Editar Plano', className: type !== "edit" ? "hide" : "btn btn-primary", onClick: () => edit(values, setValues)},
    {field: 'button', type: 'button', title: 'Voltar', id: "button_criar", label: 'Voltar', className: "btn btn-success product-list-button-margin", onClick: () => goBack()}
  ]

  return (
    <div>
      <Create fields={fields} buttonsActions={buttonsActions}/>
    </div>
  )
}

const create = async (values = {}, setValues) => {
  const isTrue = validateMapping(values)
  
  if(!isTrue){
    return
  }

  const { data } = await api.post('/plans', values)

  if(!data){
    console.log('Erro ao criar plano', data);
  }

  setValues({})
}

const edit = async (values = {}, setValues) => {
  const { id, ...dataValues} = values
  const { data } = await api.put(`/plans/${id}`, dataValues)

  if(!data){
    console.log('Erro ao editar plano', data);
  }

  setValues({})
}

const onChange = ({target: {name, value}}, values, setValues) => {
  setValues({...values, [name]: value})
}

const get = async (id, setValues) => {
  const { data } = await api.get(`/plans/${id}`)
  setValues(data)
}
