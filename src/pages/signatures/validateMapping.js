export default (values) => {
  let isTrue = true
  const objectKeys = Object.keys(values)

  if(objectKeys < 1){
    return false
  }

  // eslint-disable-next-line no-sequences
  ['plan_id', 'user_id'].forEach(element => {
    if(!objectKeys.includes(element)){
      isTrue = false
      return;
    }
  });

  return isTrue
}