import React, { useState, useEffect } from 'react'
import Create from '../../components/create'
import api from '../../utils/axios';
import './css/plan.css'
import validateMapping from './validateMapping';
import useReactRouter from 'use-react-router'

export default function Townhouse({disabled = false, type}){
  const {match: { params: { id, user_id } }, history: { goBack } }= useReactRouter()
  const [values, setValues] = useState({user_id, plan_id: ''})
  const [plans, setPlans] = useState([]);
  useEffect(() => {
    getPlans(setPlans)
    if(id){
      get(id, setValues)
    }
  }, [id])

  const fields = [
    {field: 'select', title: 'Plano', id: "plan_id", name: "plan_id", value: values.plan_id || '', disabled,  placeholder: "Selecione o plano", options: planOptions(plans), onChange: (target) => onChange(target, values, setValues)},
  ]

  const buttonsActions = [
    {field: 'button', type: 'button', title: 'Criar Plano', id: "button_criar", label: 'Criar Assinatura', className:  type !== "create" ? "hide" : "btn btn-primary", onClick: () => create(values, setValues, plans)},
    {field: 'button', type: 'button', title: 'Editar Plano', id: "button_criar", label: 'Editar Assinatura', className: type !== "edit" ? "hide" : "btn btn-primary", onClick: () => edit(values, setValues)},
    {field: 'button', type: 'button', title: 'Voltar', id: "button_criar", label: 'Voltar', className: "btn btn-success product-list-button-margin", onClick: () => goBack()}
  ]

  return (
    <div>
      <Create fields={fields} buttonsActions={buttonsActions}/>
    </div>
  )
}

const create = async (values = {}, setValues, plans) => {
  const isTrue = validateMapping(values)
  
  if(!isTrue){
    return
  }

  const plan = plans.find(plan => plan.id === values.plan_id)
  values.price = plan.price

  const { data: {data} } = await api.post('/signatures', values)

  if(!data){
    console.log('Erro ao criar assinatura', data);
  }

  setValues({})
}

const edit = async (values = {}, setValues) => {
  const { id, ...dataValues} = values
  const { data } = await api.put(`/signatures/${id}`, dataValues)

  if(!data){
    console.log('Erro ao editar assinatura', data);
  }

  setValues({})
}

const onChange = ({target: {name, value}}, values, setValues) => {
  setValues({...values, [name]: value})
}

const get = async (id, setValues) => {
  const { data } = await api.get(`/signatures/${id}`)
  setValues(data)
}

const getPlans = async (setValues) => {
  const { data: { data } } = await api.get(`/plans?getAll=true`)
  setValues(data)
}

const planOptions = (plans) => {
  return plans.map(plan => {
    return {
      value: plan.id,
      name: plan.name
    }
  })
}
