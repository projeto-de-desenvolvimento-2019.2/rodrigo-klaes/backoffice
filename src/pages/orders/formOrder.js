import React, { useState, useEffect } from 'react'
import Create from '../../components/create'
import api from '../../utils/axios';
import './css/order.css'
import validateMapping from './validateMappingOrder';
import useReactRouter from 'use-react-router'
import formatFrindlyPrice from '../../utils/formatFrindlyPrice';

export default function Order({disabled = false, type}){
  const {match: { params: { id } }, history: { goBack } }= useReactRouter()
  const [values, setValues] = useState({})
  const [products, setProducts] = useState([])
  const [pagination, setPagination] = useState({
    total: 0,
    totalPage: 0,
    page: 0,
    size: 0
  })
  
  useEffect(() => {
    if(id){
      get(id, setValues)
    }
  }, [id])

  useEffect(() => {
    getProducts(setProducts)
  }, [ ])

  const fields = [
    {field: 'input', type: 'text', title: 'Nome do usuário', id: "name", name: "name", value: values.name || '', disabled, onChange: (target) => onChange(target, values, setValues)},
  ]

  const fieldToAddProducts = [
    {
      field: 'select', 
      title: 'Produto', 
      id: "product",
      name: "product",
      classNameDiv: 'form-group col-md-6', 
      value: values.product || '', 
      disabled,
      placeholder: "Selecione o produto", 
      options: productOptions(products), 
      onChange: (target) => onChange(target, values, setValues)
    },
    {
      field: 'input', 
      type: 'number',
      title: 'Quantidade', 
      classNameDiv: 'form-group col-md-6',
      id: "amount",
      name: "amount", 
      value: values.amount || '',
      disabled,
      placeholder: "Digite a quantidade", 
      onChange: (target) => onChange(target, values, setValues)
    },
  ]

  const buttonsActions = [
    {field: 'button', type: 'button', title: 'Criar Pedido', id: "button_criar", label: 'Criar Produto', className:  type !== "create" ? "hide" : "btn btn-primary", onClick: () => create(values, setValues)},
    {field: 'button', type: 'button', title: 'Voltar', id: "button_criar", label: 'Voltar', className: "btn btn-success product-list-button-margin", onClick: () => goBack()}
  ]

  const buttonAdd = [
    {
      field: 'button', 
      type: 'button', 
      title: 'Adicionar produto', 
      id: "button_adicionar", 
      label: 'Adicionar produto', 
      className:  type !== "create" ? "hide" : "btn btn-primary", 
      onClick: target => setProduct(values, setValues, products)
    },
  ]

  return (
    <div>
      <Create fields={fields} buttonsActions={[]}/>
      <Create fields={fieldToAddProducts} buttonsActions={[]} className="product-create-box d-flex"/>
      <Create fields={[]} buttonsActions={buttonAdd}/>
      <br />
      <div className="order-show product-create-box">
        <p>
          <b>Produtos:</b> <br />
        </p>
        <ul>
          {
            values.products?.map((product, key) => (
              <li key={key}>{product.amount}x {product.name} -  R$ {product.total.toString().replace('.', ',')}</li>
            ))          
          }
        </ul>
        <p>
          <b>Total:</b> R$ {values.total?.toString().replace('.', ',') || ''} <br />
        </p>
      </div>
      <br />
      <Create fields={[]} buttonsActions={buttonsActions}/>
    </div>
  )
}

const create = async (values = {}, setValues) => {
  const isTrue = validateMapping(values)

  if(!isTrue){
    return
  }

  const { data } = await api.post('/products', values)

  if(!data){
    console.log('Erro ao criar produto', data);
  }

  setValues({})
}

const onChange = ({target: {name, value}}, values, setValues) => {
  setValues({...values, [name]: value})
}

const get = async (id, setValues) => {
  const { data } = await api.get(`/products/${id}`)
  setValues(data)
}

const setProduct = (values, setValues, products) => {

  //TODO: sweet alert
  if(values.product === '' || values.amount === '' ){
    alert('Preencha todas as informações dos produto')
    return
  }

  const productFinded = products.find(item => item.id === values.product)

  const product = {
    name: productFinded.name,
    product_id: values.product,
    amount: values.amount,
    price: productFinded.price,
    total: values.amount * productFinded.price
  }

  let total = values.total || 0
  total += product.total

  const newProducts = values.products ? [...values.products, product ] : [ product ]

  setValues({...values, amount: '', product: '', total, products: newProducts })
}

const getProducts = async (setProducts) => {
  const { data: { data } } = await api.get('/products/category?getAll=true')
  setProducts(data)
}

const productOptions = (products) => {
  return products.map(product => {
    return {
      value: product.id,
      name: product.name + ' R$ ' + formatFrindlyPrice(product.price) + "/" + product.unity 
    }
  })
}

// const createOrderProducts = (products) => {
//   const { data } 
// }