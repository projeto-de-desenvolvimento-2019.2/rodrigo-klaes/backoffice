import React, {useState, useEffect} from 'react'
import List from '../../components/list'
import api from '../../utils/axios';
import './css/order.css'
import useReactRouter from 'use-react-router'

const tableTitles = [
  {
    name: 'code',
    title: 'Código'
  },
  {
    name: 'user_name',
    title: 'Usuário'
  },
  {
    name: 'status',
    title: 'Status'
  },
  {
    name: 'status_delivery',
    title: 'Status da entrega'
  },
  {
    name: 'total',
    title: 'Total'
  },
]

export default function ListOrders(){
  const { history } = useReactRouter()

  const [orders, setOrders] = useState([])
  const [search, setSearch] = useState([])

  const [pagination, setPagination] = useState({
    total: 0,
    totalPage: 0,
    page: 1,
    size: 0
  })

  useEffect(() => {
    getOrders(setOrders, setPagination, pagination.page)
  }, [])
  
  const actions = {
    handleShow,
    handleDelete,
    getOrders
  }
  
  const buttons = (id, actions, history) => {
    return (
      <td>
        <button onClick={() => actions.handleShow(id, history)} className="btn btn-info product-list-button-margin"><i className="fa fa-eye" aria-hidden="true"></i></button>
        <button onClick={() => actions.handleDelete(id, setOrders)} className="btn btn-danger product-list-button-margin"><i className="fa fa-trash" aria-hidden="true"></i></button>
      </td>
    )
  }

  return (
    <>
      <div className="form-group">
        <input           
          type="text" 
          id="search"
          name="search"
          placeholder="Buscar"
          onChange={(target) => onChange(target, search, setSearch)}
        />
        <button className="btn btn-info product-list-button-margin" onClick={() => getOrders(setOrders, setPagination, pagination, search)}><i className="fa fa-search" aria-hidden="true"></i></button>
        <button className="btn btn-success product-list-button-margin" onClick={() => history.push('/order/create')}><i className="fa fa-plus" aria-hidden="true"></i></button>
     </div>
      <List  titles={tableTitles} body={orders} buttons={buttons} actions={actions} history={history} pagination={pagination} get={getOrders} set={{setPagination, setValues: setOrders}} search={search}/>
    </>
  )
}

const onChange = ({target: {name, value}}, values, setValues) => {
  setValues({...values, [name]: value})
}

const getOrders = async (setOrders, setPagination , page, search = '') => {
  const { data: { data, ...pageInfos } } = await api.get(`/orders/products/user?search=${search}&skip=${page || 1}`)
  const dataFormart = data.map(item => {
    const total = 'R$ ' + (item.total.toFixed(2)).toString().replace('.', ',')
    return (
      {
        user_name: item.user?.name,
        status_delivery: item.schedule ? item.schedule.status.name : '',
        ...item,
        total
      }
    )
  })

  setOrders(dataFormart);
  setPagination(pageInfos)
}

const handleShow = (id, history) => {
  history.push(`/order/show/${id}`)
}

const handleDelete = async (id, setOrders) => {
  const resposta = window.confirm('Confirma a exclusão?')
  if (resposta === false) {
    return
  }

  await api.delete(`/orders/${id}`)
  getOrders(setOrders)
}
