import React, { useEffect, useState } from 'react'
import useReactRouter from 'use-react-router'
import fieldTypes from '../../components/fieldTypes'
import api from '../../utils/axios'
import * as moment from 'moment'

const getColor = (status) => {
  if(status === 'finalizado') return 'green'
  if(status === 'cancelado') return 'red'
}

export default function OrderShow(){
  const {match: { params: { id } }, history: { goBack } }= useReactRouter()

  const buttonsActions = [
    {field: 'button', type: 'button', title: 'Voltar', id: "button_criar", label: 'Voltar', className: "btn btn-success product-list-button-margin", onClick: () => goBack()}
  ]

  const [order, setOrder] = useState({})

  useEffect(() => {
    getOrder(id, setOrder)
  }, [id])

  return (
    <div className="order-show">
      <h1><b>Pedido - {order.code}</b></h1>
      <p><b>Status:</b> <b className={getColor(order.status)}>{order.status}</b></p> {/* TODO: status maisculo*/}
      <p><b>Data do pedido:</b> {moment(order.created_at).format('DD/MM/YYYY HH:mm')}</p>
      <p><b>Usuário:</b> {order.user?.name}</p>
      <p>
        <b>Produtos:</b> <br />
      </p>
      <ul>
          {
            order.products?.map((product, key) => (
              <li key={key}>{product.name} - R$ {product.price.toString().replace('.', ',')}</li>
            ))          
          }
      </ul>
      <div>
        { 
          buttonsActions.map((field, index) => input(field.field, field, index)) 
        }
      </div>
    </div>
  )
}

const getOrder = async (id, setOrder) => {
  const { data } = await api.get(`orders/${id}/products/user`)

  setOrder(data)
}


//TODO: insolar eem um metodo util
function input(fieldType, config, index){
  return fieldTypes(fieldType)(config, index)
}