import React, {useState, useEffect} from 'react'
import List from '../../components/list'
import api from '../../utils/axios';
import './css/product.css'
import useReactRouter from 'use-react-router'

const tableTitles = [
  {
    name: 'name',
    title: 'Nome'
  },
  {
    name: 'price',
    title: 'Preço'
  },
  {
    name: 'category_name',
    title: 'Categoria'
  },
  {
    name: 'amount',
    title: 'Qntd.'
  },
  {
    name: 'unity',
    title: 'Unidade'
  },
]

export default function ListProducts(){
  const { history } = useReactRouter()

  const [products, setProducts] = useState([])
  const [search, setSearch] = useState([])
  const [pagination, setPagination] = useState({
    total: 0,
    totalPage: 0,
    page: 1,
    size: 0
  })

  useEffect(() => {
    getProducts(setProducts, setPagination, pagination.page)
  }, [pagination.page])
  
  const actions = {
    handleShow,
    handleEdit,
    handleDelete,
    getProducts
  }
  
  const buttons = (id, actions, history) => {
    return (
      <td>
        <button onClick={() => actions.handleShow(id, history)} className="btn btn-info product-list-button-margin"><i className="fa fa-eye" aria-hidden="true"></i></button>
        <button onClick={() => actions.handleEdit(id, history)} className="btn btn-success product-list-button-margin"><i className="fa fa-pencil-square-o" aria-hidden="true"></i></button>
        <button onClick={() => actions.handleDelete(id, setProducts)} className="btn btn-danger product-list-button-margin"><i className="fa fa-trash" aria-hidden="true"></i></button>
      </td>
    )
  }

  return (
    <>
      <div className="form-group">
        <input           
          type="text" 
          id="search"
          name="search"
          placeholder="Buscar"
          onChange={(target) => onChange(target, setSearch)}
        />
        <button className="btn btn-info product-list-button-margin" onClick={() => getProducts(setProducts, setPagination, pagination, search)}><i className="fa fa-search" aria-hidden="true"></i></button>
        <button className="btn btn-success product-list-button-margin" onClick={() => history.push('/product/create')}><i className="fa fa-plus" aria-hidden="true"></i></button>
     </div>
      <List  titles={tableTitles} body={products} buttons={buttons} actions={actions} history={history} pagination={pagination} get={getProducts} set={{setPagination, setValues: setProducts}} search={search} />
    </>
  )
}


const onChange = ({target: {value}}, setValues) => {
  setValues(value)
}

const getProducts = async (setProducts, setPagination, page = 1, search = '') => {
  const { data: { data, ...pageInfos },  } = await api.get(`/products/category?search=${search}&page=${page}`)
  
  const formatData = data.map(item => (
    {
      category_name: item.category.name,
      ...item,
      price: 'R$ ' + (item.price.toFixed(2)).toString().replace('.', ',')
    }
  ))

  setPagination(pageInfos)
  setProducts(formatData);
}

const handleShow = (id, history) => {
  history.push(`/product/show/${id}`)
}

const handleEdit = (id, history) => {
  history.push(`/product/edit/${id}`)
}

const handleDelete = async (id, setProducts) => {
  const resposta = window.confirm('Confirma a exclusão?')
  if (resposta === false) {
    return
  }

  await api.delete(`/products/${id}`)
  getProducts(setProducts)
}
