export default (values) => {
  let isTrue = true
  const objectKeys = Object.keys(values)

  if(objectKeys < 1){
    return false
  }

  // eslint-disable-next-line no-sequences
  ['name', 'category_id', 'code', 'price', 'unity', 'amount', 'file'].forEach(element => {
    if(!objectKeys.includes(element)){
      isTrue = false
      return;
    }
  });

  return isTrue
}