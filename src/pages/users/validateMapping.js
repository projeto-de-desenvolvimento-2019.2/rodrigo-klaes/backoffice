export default (values) => {
  let isTrue = true
  const objectKeys = Object.keys(values)

  if(objectKeys < 1){
    return false
  }

  // eslint-disable-next-line no-sequences
  ['name', 'email', 'cpf', ].forEach(element => {
    
    if(!objectKeys.includes(element)){
      isTrue = false
      alert('Preencha todos os campos')
      return;
    }
  });

  return isTrue
}