import React, {useState, useEffect} from 'react'
import List from '../../components/list'
import api from '../../utils/axios';
import './css/schedule.css'
import useReactRouter from 'use-react-router'
import * as moment from 'moment'

const URL_API = process.env.REACT_APP_ENV === 'develop' ?  process.env.REACT_APP_API_URL_DEV : process.env.REACT_APP_API_URL

const tableTitles = [
  {
    name: 'user_name',
    title: 'Usuário'
  },
  {
    name: 'order_code',
    title: 'Pedido'
  },
  {
    name: 'status',
    title: 'Status'
  },
  {
    name: 'date',
    title: 'Data de entrega'
  },
]

export default function NextDeliverySchedule(){
  const { history } = useReactRouter()

  const [values, setValues] = useState([])
  const [search, setSearch] = useState([])
  const [pagination, setPagination] = useState({
    total: 0,
    totalPage: 0,
    page: 1,
    size: 0
  })

  useEffect(() => {
    get(setValues, setPagination, pagination.page)
  }, [])
  
  const actions = {
    handleDelete,
    handleCheck,
    handleCancel
  }
  
  const buttons = (id, actions, history) => {
    return (
      <td>
        <button onClick={() => actions.handleCheck(id, setValues, setPagination)} className="btn btn-success schedule-list-button-margin"><i className="fa fa-check" aria-hidden="true"></i></button>
        <button onClick={() => actions.handleCancel(id, setValues, setPagination)} className="btn btn-secondary schedule-list-button-margin"><i className="fa fa-times-circle" aria-hidden="true"></i></button>
        <button onClick={() => actions.handleDelete(id, setValues)} className="btn btn-danger schedule-list-button-margin"><i className="fa fa-trash" aria-hidden="true"></i></button>
      </td>
    )
  }

  return (
    <>
      <div className="form-group">
        <input           
          type="text" 
          id="search"
          name="search"
          placeholder="Buscar"
          onChange={(target) => onChange(target, setSearch)}
        />
        <button className="btn btn-info schedule-list-button-margin" onClick={() => get(setValues, search)}><i className="fa fa-search" aria-hidden="true"></i></button>
        <button className="btn btn-success schedule-list-button-margin" onClick={() => history.push('/schedule/create')}><i className="fa fa-plus" aria-hidden="true"></i></button>

        <button className="btn btn-secondary user-list-button-margin" onClick={() => window.location.href = URL_API + '/schedules/export/delivery'}><i class="fa fa-file-excel"></i></button>
     </div>
      <List  titles={tableTitles} body={values} buttons={buttons} actions={actions} history={history} pagination={pagination} get={get} set={{setPagination, setValues: setValues}} search={search}/>
    </>
  )
}

const get = async (setValues, setPagination, page = 1, search = '') => {
  const { data: { data, ...pageInfos } } = await api.get(`/schedules/next/deliveries?search=${search}&page=${page}`)

  const dataFormated = data.map(item => {
    return {
      ...item,
      date: moment(item.date).format('DD/MM/YYYY HH:mm'),
      user_name: item.user.name,
      order_code: item.order.code,
      status: item.status.name
    }
  })

  setValues(dataFormated);
  setPagination(pageInfos);
}

const onChange = ({target: {value}}, setValues) => {
  setValues(value)
}

const handleDelete = async (id, setValues, ) => {
  const resposta = window.confirm('Confirma a exclusão?')
  if (resposta === false) {
    return
  }

  await api.delete(`/schedules/${id}`)  

  get(setValues)
}

const handleCheck = async (id, setValues, setPagination) => {
  const resposta = window.confirm('Deseja confirmar a entrega?')
  if (resposta === false) {
    return
  }

  await api.put(`/schedules/${id}`, {status_id: 4})

  get(setValues, setPagination)
}

const handleCancel = async (id, setValues, setPagination) => {
  const resposta = window.confirm('Deseja cancelar a entrega?')
  if (resposta === false) {
    return
  }

  await api.put(`/schedules/${id}`, {status_id: 5})

  get(setValues, setPagination)
}
