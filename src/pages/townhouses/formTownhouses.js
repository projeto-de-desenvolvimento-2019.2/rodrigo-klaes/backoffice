import React, { useState, useEffect } from 'react'
import Create from '../../components/create'
import api from '../../utils/axios';
import './css/townhouse.css'
import validateMappingTownhouse from './validateMappingTownhouse';
import useReactRouter from 'use-react-router'

export default function Townhouse({disabled = false, type}){
  const {match: { params: { id } }, history: { goBack } }= useReactRouter()
  const [values, setValues] = useState({})

  useEffect(() => {
    if(id){
      get(id, setValues)
    }
  }, [id])

  const fields = [
    {field: 'input', type: 'text', title: 'Nome do conominio', id: "name", name: "name", value: values.name || '', disabled, onChange: (target) => onChange(target, values, setValues)},
    {field: 'input', type: 'text', title: 'CEP', id: "zipcode", name: "zipcode", value: values.zipcode || '', disabled,  onChange: (target) => onChange(target, values, setValues)},
    {field: 'input', type: 'text', title: 'Endereço', id: "address", name: "address", value: values.address || '', disabled,  onChange: (target) => onChange(target, values, setValues)},
    {field: 'input', type: 'text', title: 'Número', id: "number", name: "number", value: values.number || '', disabled,  onChange: (target) => onChange(target, values, setValues)},
    {field: 'input', type: 'text', title: 'Complemento', id: "complement", name: "complement", value: values.complement || '', disabled,  onChange: (target) => onChange(target, values, setValues)},
    {field: 'input', type: 'text', title: 'Bairro', id: "district", name: "district", value: values.district || '', disabled,  onChange: (target) => onChange(target, values, setValues)},
    {field: 'input', type: 'text', title: 'Cidade', id: "city", name: "city", value: values.city || '', disabled,  onChange: (target) => onChange(target, values, setValues)},
    {field: 'input', type: 'text', title: 'Estado', id: "state", name: "state", value: values.state || '', disabled,  onChange: (target) => onChange(target, values, setValues)},
    {field: 'input', type: 'text', title: 'País', id: "country", name: "country", value: values.country || '', disabled,  onChange: (target) => onChange(target, values, setValues)},
  ]

  const buttonsActions = [
    {field: 'button', type: 'button', title: 'Criar Produto', id: "button_criar", label: 'Criar Produto', className:  type !== "create" ? "hide" : "btn btn-primary", onClick: () => create(values, setValues)},
    {field: 'button', type: 'button', title: 'Editar Produto', id: "button_criar", label: 'Editar Produto', className: type !== "edit" ? "hide" : "btn btn-primary", onClick: () => edit(values, setValues)},
    {field: 'button', type: 'button', title: 'Voltar', id: "button_criar", label: 'Voltar', className: "btn btn-success product-list-button-margin", onClick: () => goBack()}
  ]

  return (
    <div>
      <Create fields={fields} buttonsActions={buttonsActions}/>
    </div>
  )
}

const create = async (values = {}, setValues) => {
  const isTrue = validateMappingTownhouse(values)
  
  if(!isTrue){
    return
  }

  const { data } = await api.post('/townhouses', values)

  if(!data){
    console.log('Erro ao criar condominios', data);
  }

  setValues({})
}

const edit = async (values = {}, setValues) => {
  const { id, ...dataValues} = values
  const { data } = await api.put(`/townhouses/${id}`, dataValues)

  if(!data){
    console.log('Erro ao editar condominios', data);
  }

  setValues({})
}

const onChange = ({target: {name, value}}, values, setValues) => {
  setValues({...values, [name]: value})
}

const get = async (id, setValues) => {
  const { data } = await api.get(`/townhouses/${id}`)
  setValues(data)
}
