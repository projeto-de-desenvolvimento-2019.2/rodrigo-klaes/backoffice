import React, {useState, useEffect} from 'react'
import List from '../../components/list'
import api from '../../utils/axios';
import './css/townhouse.css'
import useReactRouter from 'use-react-router'

const tableTitles = [
  {
    name: 'name',
    title: 'Nome'
  },
  {
    name: 'address',
    title: 'Endereço'
  },
  {
    name: 'city',
    title: 'Cidade'
  },
]

export default function ListTownhouses(){
  const { history } = useReactRouter()

  const [values, setValues] = useState([])
  const [search, setSearch] = useState([])
  const [pagination, setPagination] = useState({
    total: 0,
    totalPage: 0,
    page: 1,
    size: 0
  })

  useEffect(() => {
    get(setValues, setPagination, pagination.page)
  }, [])
  
  const actions = {
    handleShow,
    handleEdit,
    handleDelete
  }
  
  const buttons = (id, actions, history) => {
    return (
      <td>
        <button onClick={() => actions.handleShow(id, history)} className="btn btn-info townhouse-list-button-margin"><i className="fa fa-eye" aria-hidden="true"></i></button>
        <button onClick={() => actions.handleEdit(id, history)} className="btn btn-success townhouse-list-button-margin"><i className="fa fa-pencil-square-o" aria-hidden="true"></i></button>
        <button onClick={() => actions.handleDelete(id, setValues)} className="btn btn-danger townhouse-list-button-margin"><i className="fa fa-trash" aria-hidden="true"></i></button>
      </td>
    )
  }

  return (
    <>
      <div className="form-group">
        <input           
          type="text" 
          id="search"
          name="search"
          placeholder="Buscar"
          onChange={(target) => onChange(target, setSearch)}
        />
        <button className="btn btn-info townhouse-list-button-margin" onClick={() => get(setValues, search)}><i className="fa fa-search" aria-hidden="true"></i></button>
        <button className="btn btn-success townhouse-list-button-margin" onClick={() => history.push('/townhouse/create')}><i className="fa fa-plus" aria-hidden="true"></i></button>
     </div>
      <List  titles={tableTitles} body={values} buttons={buttons} actions={actions} history={history} pagination={pagination} get={get} set={{setPagination, setValues: setValues}} search={search}/>
    </>
  )
}

const get = async (setValues, setPagination, page, search = '') => {
  const { data: { data, ...pageInfos } } = await api.get(`townhouses?search=${search}&page=${page}`)

  setValues(data);
  setPagination(pageInfos);
}

const onChange = ({target: {value}}, setValues) => {
  setValues(value)
}

const handleShow = (id, history) => {
  history.push(`/townhouse/show/${id}`)
}

const handleEdit = (id, history) => {
  history.push(`/townhouse/edit/${id}`)
}

const handleDelete = async (id, setValues) => {
  const resposta = window.confirm('Confirma a exclusão?')
  if (resposta === false) {
    return
  }

  await api.delete(`/townhouses/${id}`)
  get(setValues)
}
