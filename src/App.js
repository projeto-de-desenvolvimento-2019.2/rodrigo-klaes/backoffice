import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import './App.css';
// import './libs/font-awesome-4.7.0/css/font-awesome.css';
import Login from './pages/login';
import Routes from './routes'

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/">
            <Routes />
          </Route>
        </Switch>
      </Router>
    </>
  );
}

export default App;
