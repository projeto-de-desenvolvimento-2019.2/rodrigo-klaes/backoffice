import React from 'react'
import {
    Switch,
    Route,
    Link
} from "react-router-dom";
  
import useReactRouter from 'use-react-router'

import Login from './pages/login'
import ProductList from './pages/products/listProducts'
import ProductCreate from './pages/products/formProduct'

import TownhouseList from './pages/townhouses/listTownhouses'
import TownhouseForm from './pages/townhouses/formTownhouses'

import PlanList from './pages/plans/listPlans'
import PlanForm from './pages/plans/formPlans'

import SignatureList from './pages/signatures/listSignature'
import SignatureForm from './pages/signatures/formSignature'

import UserList from './pages/users/listUsers'
import UserForm from './pages/users/formUsers'

import OrderList from './pages/orders/listOrder'
import OrderShow from './pages/orders/showOrder'
import OrderCreate from './pages/orders/formOrder'

import ScheduleList from './pages/schedules/listSchedules'
import NextDeliveriesList from './pages/schedules/listNextDeliverys'
// import OrderShow from './pages/orders/showOrder'
// import OrderCreate from './pages/orders/formOrder'


export default function Routes(){
  const { history } = useReactRouter()

  const access_token = localStorage.getItem('access_token')

  if (!access_token) {
    history.push('/login')
  }

 return (
   <>
    <nav>
      <Link to="/product/list">Produtos</Link>
      <Link to="/order/list">Pedidos</Link>
      <Link to="/townhouse/list">Condomínios</Link>
      <Link to="/user/list">Usuários</Link>
      <Link to="/plan/list">Planos</Link>
      <Link to="/schedule/list">Agendas</Link>
      <Link to="/schedule/next/list">Próximas entregas</Link>
    </nav>
    <div className="container-view">
      <Switch>

       

        <Route path="/login">
          <Login />
        </Route>

        {/** Products */}
        <Route path="/schedule/list">
          <ScheduleList />
        </Route>
        <Route path="/schedule/next/list">
          <NextDeliveriesList />
        </Route>
        {/* <Route path="/product/create">
          <ProductCreate type="create" />
        </Route>
        <Route path="/product/edit/:id">
          <ProductCreate type="edit" />
        </Route>
        <Route path="/product/show/:id">
          <ProductCreate disabled={true} />
        </Route> */}

        {/** Products */}
        <Route path="/product/list">
          <ProductList />
        </Route>
        <Route path="/product/create">
          <ProductCreate type="create" />
        </Route>
        <Route path="/product/edit/:id">
          <ProductCreate type="edit" />
        </Route>
        <Route path="/product/show/:id">
          <ProductCreate disabled={true} />
        </Route>

        {/** Planos */}
        <Route path="/plan/list">
          <PlanList />
        </Route>
        <Route path="/plan/create">
          <PlanForm type='create' />
        </Route>
        <Route path="/plan/edit/:id">
          <PlanForm type='edit' />
        </Route>
        <Route path="/plan/show/:id">
          <PlanForm disabled={true} />
        </Route>

        {/** Assinaturas */}
        <Route path="/signature/list/:user_id">
          <SignatureList />
        </Route>
        <Route path="/signature/create/:user_id">
          <SignatureForm type='create' />
        </Route>
        <Route path="/signature/edit/:id">
          <SignatureForm type='edit' />
        </Route>
        <Route path="/signature/show/:id">
          <SignatureForm disabled={true} />
        </Route>

        {/** Orders */}
        <Route path="/order/list">
          <OrderList />
        </Route>
        <Route path="/order/create">
          <OrderCreate type="create" />
        </Route>
        <Route path="/order/show/:id">
          <OrderShow />
        </Route>

        {/** Townhouses */}
        <Route path="/townhouse/list">
          <TownhouseList />
        </Route>
        <Route path="/townhouse/create">
          <TownhouseForm type="create" />
        </Route>
        <Route path="/townhouse/edit/:id">
          <TownhouseForm type="edit" />
        </Route>
        <Route path="/townhouse/show/:id">
          <TownhouseForm disabled={true} />
        </Route>

        {/** Users */}
        <Route path="/user/list">
          <UserList />
        </Route>
        <Route path="/user/create">
          <UserForm type="create" />
        </Route>
        <Route path="/user/edit/:id">
          <UserForm type="edit" />
        </Route>
        <Route path="/user/show/:id">
          <UserForm disabled={true} />
        </Route>
      </Switch>
    </div>
    </>
 )
}