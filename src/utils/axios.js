import axios from 'axios';

const URL_API = process.env.REACT_APP_ENV === 'develop' ?  process.env.REACT_APP_API_URL_DEV : process.env.REACT_APP_API_URL

const api = axios.create({
    baseURL: URL_API,
})

api.interceptors.request.use(function (config) {
    const access_token = window.localStorage.getItem('access_token')
    const role = window.localStorage.getItem('role')
    config.headers.Authorization =  'Bearer ' + access_token;
    config.headers.role = role;

    return config;
});

api.interceptors.response.use(res => res, function (error) {
    console.log(error);
    if(error.response.status === 401){
        window.localStorage.setItem('access_token', '')
        window.location.href = '/login'
    }
    return error;
});

export default api