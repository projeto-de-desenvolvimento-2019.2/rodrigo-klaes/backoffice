const formatFrindlyPrice = (price) => {
    return price.toString().replace('.', ',');
}

export default formatFrindlyPrice